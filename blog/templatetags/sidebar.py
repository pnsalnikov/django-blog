from django import template
from django.db.models import Count
from blog.models import Post, Tag


register = template.Library()


@register.inclusion_tag("blog/popular_posts.html")
def get_popular_posts(posts_count=3):
    posts = Post.objects.order_by('-views')[:posts_count]
    return {"posts": posts}


@register.inclusion_tag("blog/tags.html")
def get_tags():
    tags = Tag.objects.all()
    return {"tags": tags}


@register.inclusion_tag("blog/popular_tags.html")
def get_popular_tags(tags_count=3):
    tags = Tag.objects.all().annotate(posts_count=Count(
        'posts')).order_by('-posts_count')[:tags_count]
    return {"tags": tags}
